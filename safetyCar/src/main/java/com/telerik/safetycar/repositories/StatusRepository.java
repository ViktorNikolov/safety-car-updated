package com.telerik.safetycar.repositories;

import com.telerik.safetycar.models.Status;

import java.util.List;

public interface StatusRepository {

    List<Status> getAll();

    Status getById(int id);

    void create(Status status);

}
