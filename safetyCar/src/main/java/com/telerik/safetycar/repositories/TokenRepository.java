package com.telerik.safetycar.repositories;

import com.telerik.safetycar.models.Token;
import com.telerik.safetycar.models.User;

public interface TokenRepository {

    Token findByToken(String token);

    Token findByUser(User user);

    void save(Token token);

}
