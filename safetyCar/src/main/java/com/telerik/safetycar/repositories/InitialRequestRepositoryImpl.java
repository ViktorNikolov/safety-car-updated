package com.telerik.safetycar.repositories;

import com.telerik.safetycar.exceptions.EntityNotFoundException;
import com.telerik.safetycar.models.InitialRequest;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class InitialRequestRepositoryImpl implements InitialRequestRepository {

    private final SessionFactory sessionFactory;

    @Autowired
    public InitialRequestRepositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }


    @Override
    public void createInitialRequest(InitialRequest initialRequest) {
        try (Session session = sessionFactory.openSession()) {
            session.save(initialRequest);
        }
    }

    @Override
    public InitialRequest getById(int id) {
        try (Session session = sessionFactory.openSession()) {
            InitialRequest InitialRequest = session.get(InitialRequest.class, id);
            if (InitialRequest == null) {
                throw new EntityNotFoundException(
                        String.format("Initial request with id %d not found!", id));
            }
            return InitialRequest;
        }
    }
}
