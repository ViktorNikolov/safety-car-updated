package com.telerik.safetycar.repositories;

import com.telerik.safetycar.exceptions.InvalidOperationException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class BaseQuoteRepositoryImpl implements BaseQuoteRepository {
    private final SessionFactory sessionFactory;

    @Autowired
    public BaseQuoteRepositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public double getBaseAmount(int cubicCapacity, int carAge) {
        try (Session session = sessionFactory.openSession()) {

            String hql = "Select baseAmount from BaseQuote" +
                    " where cubicCapacityMin <=" + cubicCapacity +
                    "and cubicCapacityMax>=" + cubicCapacity +
                    "and carAgeMin <=" + carAge +
                    "and carAgeMax >=" + carAge;

            Query<Double> query = session.createQuery(hql, Double.class);
            if (query.list().size() == 0)
                throw new InvalidOperationException("Error");
            return query.list().get(0);
        }
    }
}
