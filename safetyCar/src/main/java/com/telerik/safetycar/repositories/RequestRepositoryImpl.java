package com.telerik.safetycar.repositories;

import com.telerik.safetycar.models.Request;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;

@Repository
public class RequestRepositoryImpl implements RequestRepository {
    private final SessionFactory sessionFactory;

    @Autowired
    public RequestRepositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public void create(Request request, String username) {
        try (Session session = sessionFactory.openSession()) {
            request.setUsername(username);
            session.save(request);
        }
    }

    @Override
    public List<Request> getAll() {
        try (Session session = sessionFactory.openSession()) {
            Query<Request> query = session.createQuery("from Request ", Request.class);
            return query.list();
        }
    }

    @Override
    public List<Request> getAllPending() {
        try (Session session = sessionFactory.openSession()) {
            Query<Request> query = session.createQuery("from Request where status.id = 1", Request.class);
            return query.list();
        }
    }

    @Override
    public Request getById(int id) {
        try (Session session = sessionFactory.openSession()) {
            return session.get(Request.class, id);
        }
    }

    @Override
    public List<Request> getByUsername(String username) {
        try (Session session = sessionFactory.openSession()) {
            Query<Request> query = session.createQuery(
                    "from Request where username=:username order by startPolicy", Request.class);
            query.setParameter("username", username);

            if (query.list().size() == 0 || username == null) {
                return new ArrayList<>();
            }
            return query.list();
        }
    }

    @Override
    public List<Request> sortByDate() {
        try (Session session = sessionFactory.openSession()) {
            Query<Request> query = session.createQuery("from Request where status.id = 1 order by startPolicy ", Request.class);
            return query.list();
        }
    }

    @Override
    public Request update(Request request) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.update(request);
            session.getTransaction().commit();
        }
        return request;
    }
}
