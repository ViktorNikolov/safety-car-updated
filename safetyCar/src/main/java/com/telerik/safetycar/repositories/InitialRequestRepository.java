package com.telerik.safetycar.repositories;

import com.telerik.safetycar.models.InitialRequest;

public interface InitialRequestRepository {

    void createInitialRequest(InitialRequest initialRequest);

    InitialRequest getById(int id);
}
