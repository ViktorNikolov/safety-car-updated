package com.telerik.safetycar.repositories;

import com.telerik.safetycar.exceptions.EntityNotFoundException;
import com.telerik.safetycar.models.CarBasic;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class CarBasicRepositoryImpl implements CarBasicRepository {

    private final SessionFactory sessionFactory;

    @Autowired
    public CarBasicRepositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }


    @Override
    public void create(CarBasic carBasic) {
        try (Session session = sessionFactory.openSession()) {
            session.save(carBasic);
        }
    }

    @Override
    public CarBasic getById(int id) {
        try (Session session = sessionFactory.openSession()) {
            CarBasic carBasic = session.get(CarBasic.class, id);
            if (carBasic == null) {
                throw new EntityNotFoundException(
                        String.format("Car with id %d not found!", id));
            }
            return carBasic;
        }
    }


}
