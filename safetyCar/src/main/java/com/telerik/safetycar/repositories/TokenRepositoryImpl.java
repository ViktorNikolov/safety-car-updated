package com.telerik.safetycar.repositories;

import com.telerik.safetycar.exceptions.EntityNotFoundException;
import com.telerik.safetycar.models.Token;
import com.telerik.safetycar.models.User;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;


@Repository
public class TokenRepositoryImpl implements TokenRepository {

    private final SessionFactory sessionFactory;

    @Autowired
    public TokenRepositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }


    @Override
    public Token findByToken(String token) {
        try (Session session = sessionFactory.openSession()) {
            Query<Token> query = session.createQuery("from Token where token = :token", Token.class);
            query.setParameter("token", token);

            List<Token> result = query.getResultList();

            if (result.isEmpty()) {
                throw new EntityNotFoundException("Missing token");
            }
            return result.get(0);
        }
    }

    @Override
    public Token findByUser(User user) {
        try (Session session = sessionFactory.openSession()) {
            Query<Token> query = session.createQuery("from Token where user = :user", Token.class);
            query.setParameter("user", user);

            List<Token> result = query.getResultList();

            if (result.isEmpty()) {
                throw new EntityNotFoundException("Missing token");
            }
            return result.get(0);
        }
    }

    @Override
    public void save(Token token) {
        try (Session session = sessionFactory.openSession()) {
            session.save(token);
        }
    }
}
