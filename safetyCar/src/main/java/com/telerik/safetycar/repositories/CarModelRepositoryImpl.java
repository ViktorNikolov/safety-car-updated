package com.telerik.safetycar.repositories;

import com.telerik.safetycar.exceptions.EntityNotFoundException;
import com.telerik.safetycar.models.CarModel;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.TypeMismatchException;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.util.List;

@Repository
@Transactional
public class CarModelRepositoryImpl implements CarModelRepository {

    private final SessionFactory sessionFactory;

    @Autowired
    public CarModelRepositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public CarModel getById(int id) {
        try (Session session = sessionFactory.openSession()) {
            CarModel carModel = session.get(CarModel.class, id);
            if (carModel == null) {
                throw new EntityNotFoundException(
                        String.format("Car model with id %d not found!", id));
            }
            return carModel;
        }
    }

    @Override
    public List<CarModel> getAll() {
        try (Session session = sessionFactory.openSession()) {
            Query<CarModel> query = session.createQuery("from CarModel ", CarModel.class);
            return query.list();
        }
    }

    @Override
    public List<CarModel> getByMakeId(int makeId) {
        try (Session session = sessionFactory.openSession()) {
            Query<CarModel> query = session.createQuery("from CarModel where carMake.id = :makeId ", CarModel.class);
            query.setParameter("makeId", makeId);
            if (query == null) {
                throw new EntityNotFoundException(
                        "There are no models for car make in the system!");
            }
            return query.list();
        } catch (TypeMismatchException e) {
            throw new EntityNotFoundException(
                    "There are no models for car make in the system!");
        }

    }
}
