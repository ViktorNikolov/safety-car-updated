package com.telerik.safetycar.repositories;

import com.telerik.safetycar.models.CarBasic;

public interface CarBasicRepository {

    void create(CarBasic carBasic);

    CarBasic getById(int id);
}
