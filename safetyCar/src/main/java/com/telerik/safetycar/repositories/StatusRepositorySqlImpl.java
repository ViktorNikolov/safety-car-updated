package com.telerik.safetycar.repositories;

import com.telerik.safetycar.models.Status;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class StatusRepositorySqlImpl implements StatusRepository {
    private final SessionFactory sessionFactory;

    public StatusRepositorySqlImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public List<Status> getAll() {
        try (Session session = sessionFactory.openSession()) {
            Query<Status> query = session.createQuery("from Status ", Status.class);
            return query.list();
        }
    }

    @Override
    public Status getById(int id) {
        try (Session session = sessionFactory.openSession()) {
            return session.get(Status.class, id);
        }
    }

    @Override
    public void create(Status status) {
        try (Session session = sessionFactory.openSession()) {
            session.save(status);
        }
    }
}
