package com.telerik.safetycar.repositories;

import com.telerik.safetycar.models.CarMake;

import java.util.List;

public interface CarMakeRepository {

    CarMake getById(int id);

    List<CarMake> getAll();
}
