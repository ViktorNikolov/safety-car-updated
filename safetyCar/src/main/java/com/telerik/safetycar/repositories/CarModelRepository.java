package com.telerik.safetycar.repositories;

import com.telerik.safetycar.models.CarModel;

import java.util.List;

public interface CarModelRepository {

    CarModel getById(int id);

    List<CarModel> getAll();

    List<CarModel> getByMakeId(int id);
}
