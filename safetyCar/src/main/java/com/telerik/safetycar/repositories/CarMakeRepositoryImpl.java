package com.telerik.safetycar.repositories;

import com.telerik.safetycar.exceptions.EntityNotFoundException;
import com.telerik.safetycar.models.CarMake;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.util.List;

@Repository
@Transactional
public class CarMakeRepositoryImpl implements CarMakeRepository {
    private final SessionFactory sessionFactory;

    @Autowired
    public CarMakeRepositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public CarMake getById(int id) {
        try (Session session = sessionFactory.openSession()) {
            CarMake carMake = session.get(CarMake.class, id);
            if (carMake == null) {
                throw new EntityNotFoundException(
                        String.format("Car make with id %d not found!", id));
            }
            return carMake;
        }
    }

    @Override
    public List<CarMake> getAll() {
        try (Session session = sessionFactory.openSession()) {
            Query<CarMake> query = session.createQuery("from CarMake ", CarMake.class);
            return query.list();
        }
    }
}
