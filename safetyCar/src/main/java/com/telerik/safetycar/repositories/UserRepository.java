package com.telerik.safetycar.repositories;

import com.telerik.safetycar.models.User;

import java.util.List;

public interface UserRepository {

    List<User> getAll();

    User getByUsername(String username);

    boolean existByUsername(String username);

    void update(User user);

}
