package com.telerik.safetycar.repositories;

public interface BaseQuoteRepository {

    double getBaseAmount(int driverAge, int carAge);
}
