package com.telerik.safetycar.repositories;

import com.telerik.safetycar.models.Request;

import java.util.List;

public interface RequestRepository {

    void create(Request request, String username);

    List<Request> getAll();

    List<Request> getAllPending();

    List<Request> getByUsername(String username);

    Request getById(int id);

    List<Request> sortByDate();

    Request update(Request request);

}
