package com.telerik.safetycar.models.utils;

import com.telerik.safetycar.models.*;
import com.telerik.safetycar.models.dto.InitialRequestDTO;
import com.telerik.safetycar.services.CarBasicService;
import com.telerik.safetycar.services.CarMakeService;
import com.telerik.safetycar.services.CarModelService;
import com.telerik.safetycar.services.InitialRequestService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class InitialRequestMapper {
    private final CarModelService modelService;
    private final CarBasicService carBasicService;
    private final InitialRequestService initialRequestService;


    @Autowired
    public InitialRequestMapper(CarModelService modelService,
                                CarBasicService carBasicService,
                                InitialRequestService initialRequestService) {
        this.modelService = modelService;
        this.carBasicService = carBasicService;
        this.initialRequestService = initialRequestService;
    }


    private CarBasic fromDto(InitialRequestDTO initialRequestDTO) {

        CarModel carModel = modelService.getById(initialRequestDTO.getModelId());
        CarBasic carBasic = new CarBasic();
        carBasic.setModel(carModel);
        carBasic.setCubicCapacity(initialRequestDTO.getCubicCapacity());
        carBasic.setYearOfRegistration(initialRequestDTO.getYearOfRegistration());

        return carBasic;
    }

    public InitialRequest fromQuoteDTO(InitialRequestDTO initialRequestDTO) {
        CarBasic carBasic = fromDto(initialRequestDTO);
        carBasicService.create(carBasic);

        InitialRequest initialRequest = new InitialRequest();
        initialRequest.setCarBasic(carBasic);
        initialRequest.setAccident(initialRequestDTO.isHadAccidents());
        initialRequest.setDriverAge(initialRequestDTO.getAge());
        initialRequest.setPremium(initialRequestDTO.getPremium());
        initialRequestService.createInitialRequest(initialRequest);

        return initialRequest;
    }
}
