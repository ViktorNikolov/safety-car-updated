package com.telerik.safetycar.models.dto;

public class UpdateDTO {

    private String password;

    public UpdateDTO() {
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

}
