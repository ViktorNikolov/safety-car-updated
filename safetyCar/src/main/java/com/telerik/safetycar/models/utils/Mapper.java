package com.telerik.safetycar.models.utils;

import com.telerik.safetycar.models.Request;
import com.telerik.safetycar.models.dto.RequestDTO;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.security.Principal;

@Component
public class Mapper {

    public static Request requestFromDTO(RequestDTO requestDTO, Principal principal) throws IOException {
        Request request = new Request();

        request.setUsername(principal.getName());
        request.setStartPolicy(requestDTO.getStartPolicy());

        MultipartFile multipartFile = requestDTO.getCarPicture();
        request.setCarPicture(multipartFile.getBytes());
        request.setName(requestDTO.getName());
        request.setEmail(requestDTO.getEmail());
        request.setPhone(requestDTO.getPhone());
        request.setAddress(requestDTO.getAddress());

        return request;
    }
}
