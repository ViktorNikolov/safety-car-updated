package com.telerik.safetycar.models.utils;

import com.telerik.safetycar.models.CarBasic;
import com.telerik.safetycar.models.Request;
import com.telerik.safetycar.models.dto.RequestDTO;
import com.telerik.safetycar.services.*;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.security.Principal;

@Component
public class RequestDisplayMapper {

    public RequestDTO toDto(int id, RequestDTO requestDTO, Principal principal,
                            RequestService requestService, StatusService statusService,
                            CarMakeService carMakeService, CarModelService carModelService,
                            CarBasicService carBasicService,
                            InitialRequestService initialRequestService) throws IOException {

        Request request = requestService.getById(id, principal.getName());
        int idInitial = request.getInitialRequest().getId();
        int idCarBasic = initialRequestService.getById(idInitial).getCarBasic().getId();

        CarBasic carBasic = carBasicService.getById(idCarBasic);

        int cc = carBasicService.getById(idCarBasic).getCubicCapacity();
        String fr = carBasicService.getById(idCarBasic).getYearOfRegistration();
        String modelCar = carModelService.getById(carBasic.getModel().getId()).getModelName();

        String makeCar = carMakeService.getById(carBasic.getModel().getCarMake().getId()).getMakeName();

        int age = initialRequestService.getById(idInitial).getDriverAge();
        boolean accident = initialRequestService.getById(idInitial).isAccident();
        String status = statusService.getById(request.getStatus().getId()).getName();
        double quote = initialRequestService.getById(idInitial).getPremium();

        requestDTO.setUsername(request.getUsername());
        requestDTO.setCarMake(makeCar);
        requestDTO.setCarModel(modelCar);
        requestDTO.setCubicCapacity(cc);
        requestDTO.setRegistrationDate(fr);
        requestDTO.setDriverAge(age);
        requestDTO.setAccident(accident);
        requestDTO.setStartPolicy(request.getStartPolicy());
        requestDTO.setName(request.getName());
        requestDTO.setEmail(request.getEmail());
        requestDTO.setPhone(request.getPhone());
        requestDTO.setAddress(request.getAddress());
        requestDTO.setStatusId(request.getStatus().getId());
        requestDTO.setStatus(status);
        requestDTO.setQuote(String.format("%.2f", quote));

        return requestDTO;
    }


}
