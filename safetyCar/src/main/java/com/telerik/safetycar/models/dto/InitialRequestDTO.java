package com.telerik.safetycar.models.dto;

import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;
import javax.validation.constraints.PositiveOrZero;


public class InitialRequestDTO {

    @Positive(message = "Please, enter car make.")
    private Integer makeId;

    @Positive
    @NotNull(message = "Please, enter car model.")
    private Integer modelId;

    @NotNull(message = "Please, enter cubic capacity.")
    @PositiveOrZero
    private Integer cubicCapacity;

    @NotEmpty(message = "Please, enter registration date.")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private String yearOfRegistration;

    @NotNull(message = "Please, enter your age.")
    @PositiveOrZero
    private Integer age;

    private boolean hadAccidents;

    private double premium;

    private String premiumView;

    public InitialRequestDTO() {
    }

    public Integer getCubicCapacity() {
        return cubicCapacity;
    }

    public void setCubicCapacity(Integer cubicCapacity) {
        this.cubicCapacity = cubicCapacity;
    }

    public String getYearOfRegistration() {
        return yearOfRegistration;
    }

    public void setYearOfRegistration(String yearOfRegistration) {
        this.yearOfRegistration = yearOfRegistration;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public boolean isHadAccidents() {
        return hadAccidents;
    }

    public void setHadAccidents(boolean hadAccidents) {
        this.hadAccidents = hadAccidents;
    }

    public Integer getMakeId() {
        return makeId;
    }

    public void setMakeId(Integer makeId) {
        this.makeId = makeId;
    }

    public double getPremium() {
        return premium;
    }

    public void setPremium(double premium) {
        this.premium = premium;
    }

    public Integer getModelId() {
        return modelId;
    }

    public void setModelId(Integer modelId) {
        this.modelId = modelId;
    }

    public String getPremiumView() {
        return premiumView;
    }

    public void setPremiumView(String premiumView) {
        this.premiumView = premiumView;
    }
}
