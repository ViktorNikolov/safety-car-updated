package com.telerik.safetycar.models.dto;

import javax.validation.constraints.*;

public class UserRegistrationDTO {

    @NotEmpty
    @Email
    private String username;

    @NotEmpty
    @Size(min = 3, message = "Password is required")
    private String password;

    @NotEmpty
    private String passwordConfirmation;

    public UserRegistrationDTO() {
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getPasswordConfirmation() {
        return passwordConfirmation;
    }

    public void setPasswordConfirmation(String passwordConfirmation) {
        this.passwordConfirmation = passwordConfirmation;
    }
}