package com.telerik.safetycar.models;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "base_quote")
public class BaseQuote {

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(name = "cc_min")
    private int cubicCapacityMin;

    @Column(name = "cc_max")
    private int cubicCapacityMax;

    @Column(name = "car_age_min")
    private int carAgeMin;

    @Column(name = "car_age_max")
    private int carAgeMax;

    @Column(name = "base_amount")
    public double baseAmount;

    public BaseQuote() {
    }

    public int getId() {
        return id;
    }

    public int getCubicCapacityMin() {
        return cubicCapacityMin;
    }

    public int getCubicCapacityMax() {
        return cubicCapacityMax;
    }

    public int getCarAgeMin() {
        return carAgeMin;
    }

    public int getCarAgeMax() {
        return carAgeMax;
    }

    public double getBaseAmount() {
        return baseAmount;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        BaseQuote baseQuote = (BaseQuote) o;
        return id == baseQuote.id &&
                cubicCapacityMin == baseQuote.cubicCapacityMin &&
                cubicCapacityMax == baseQuote.cubicCapacityMax &&
                carAgeMin == baseQuote.carAgeMin &&
                carAgeMax == baseQuote.carAgeMax &&
                Double.compare(baseQuote.baseAmount, baseAmount) == 0;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, cubicCapacityMin, cubicCapacityMax, carAgeMin, carAgeMax, baseAmount);
    }
}
