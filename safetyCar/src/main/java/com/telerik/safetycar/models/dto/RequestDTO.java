package com.telerik.safetycar.models.dto;

import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

public class RequestDTO {

    private int initialRequestId;

    private String username;

    private String carMake;

    private String carModel;

    private int cubicCapacity;

    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private String registrationDate;

    private int driverAge;

    private boolean accident = false;

    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @NotEmpty(message = "Please, enter policy start date.")
    private String startPolicy;

    @NotNull
    private MultipartFile carPicture;

    @NotEmpty(message = "Please enter full name.")
    private String name;

    @NotEmpty(message = "Please, enter a valid email.")
    private String email;

    @NotNull(message = "Please, enter your phone number.")
    private Integer phone;

    @NotEmpty(message = "Please, enter your address.")
    private String address;

    private String status;

    private int statusId;

    private String quote;

    public int getInitialRequestId() {
        return initialRequestId;
    }

    public void setInitialRequestId(int initialRequestId) {
        this.initialRequestId = initialRequestId;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getCarMake() {
        return carMake;
    }

    public void setCarMake(String carMake) {
        this.carMake = carMake;
    }

    public String getCarModel() {
        return carModel;
    }

    public void setCarModel(String carModel) {
        this.carModel = carModel;
    }

    public int getCubicCapacity() {
        return cubicCapacity;
    }

    public void setCubicCapacity(int cubicCapacity) {
        this.cubicCapacity = cubicCapacity;
    }

    public String getRegistrationDate() {
        return registrationDate;
    }

    public void setRegistrationDate(String registrationDate) {
        this.registrationDate = registrationDate;
    }

    public int getDriverAge() {
        return driverAge;
    }

    public void setDriverAge(int driverAge) {
        this.driverAge = driverAge;
    }

    public boolean isAccident() {
        return accident;
    }

    public void setAccident(boolean accident) {
        this.accident = accident;
    }

    public String getStartPolicy() {
        return startPolicy;
    }

    public void setStartPolicy(String startPolicy) {
        this.startPolicy = startPolicy;
    }

    public MultipartFile getCarPicture() {
        return carPicture;
    }

    public void setCarPicture(MultipartFile carPicture) {
        this.carPicture = carPicture;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Integer getPhone() {
        return phone;
    }

    public void setPhone(Integer phone) {
        this.phone = phone;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public int getStatusId() {
        return statusId;
    }

    public void setStatusId(int statusId) {
        this.statusId = statusId;
    }

    public String getQuote() {
        return quote;
    }

    public void setQuote(String quote) {
        this.quote = quote;
    }
}

