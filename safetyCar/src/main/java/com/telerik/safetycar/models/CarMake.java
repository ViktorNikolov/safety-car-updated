package com.telerik.safetycar.models;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "cars_makes")
public class CarMake {

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(name = "name")
    private String makeName;

    public CarMake() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getMakeName() {
        return makeName;
    }

    public void setMakeName(String make_name) {
        this.makeName = make_name;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CarMake carMake = (CarMake) o;
        return id == carMake.id &&
                Objects.equals(makeName, carMake.makeName);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, makeName);
    }

}
