package com.telerik.safetycar.models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import javax.validation.constraints.Email;
import java.util.Arrays;
import java.util.Objects;

@Entity
@Table(name = "requests")
public class Request {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;

    @Column(name = "username")
    private String username;

    @OneToOne
    @JoinColumn(name = "initial_request_id")
    private InitialRequest initialRequest;

    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @Column(name = "start_policy")
    private String startPolicy;

    @JsonIgnore
    @Column(name = "car_picture")
    private byte[] carPicture;

    @Column(name = "name")
    private String name;

    @Column(name = "email")
    @Email
    private String email;

    @Column(name = "phone")
    private int phone;

    @Column(name = "address")
    private String address;

    @ManyToOne
    @JoinColumn(name = "status_id")
    private Status status;

    public Request() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public InitialRequest getInitialRequest() {
        return initialRequest;
    }

    public void setInitialRequest(InitialRequest initialRequest) {
        this.initialRequest = initialRequest;
    }

    public void setStartPolicy(String startPolicy) {
        this.startPolicy = startPolicy;
    }

    public String getStartPolicy() {
        return startPolicy;
    }

    public byte[] getCarPicture() {
        return carPicture;
    }

    public void setCarPicture(byte[] carPicture) {
        this.carPicture = carPicture;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public int getPhone() {
        return phone;
    }

    public void setPhone(int phone) {
        this.phone = phone;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Request request = (Request) o;
        return id == request.id &&
                phone == request.phone &&
                Objects.equals(username, request.username) &&
                Objects.equals(initialRequest, request.initialRequest) &&
                Objects.equals(startPolicy, request.startPolicy) &&
                Arrays.equals(carPicture, request.carPicture) &&
                Objects.equals(name, request.name) &&
                Objects.equals(email, request.email) &&
                Objects.equals(address, request.address) &&
                Objects.equals(status, request.status);
    }

    @Override
    public int hashCode() {
        int result = Objects.hash(id, username, initialRequest, startPolicy, name, email, phone, address, status);
        result = 31 * result + Arrays.hashCode(carPicture);
        return result;
    }
}
