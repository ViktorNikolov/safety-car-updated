package com.telerik.safetycar.models;


import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "cars_basics")
public class CarBasic {

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @ManyToOne
    @JoinColumn(name = "model_id")
    private CarModel model;

    @Column(name = "cubic_capacity")
    private int cubicCapacity;

    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @Column(name = "first_registration_date")
    private String yearOfRegistration;

    public CarBasic() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public CarModel getModel() {
        return model;
    }

    public void setModel(CarModel model) {
        this.model = model;
    }

    public int getCubicCapacity() {
        return cubicCapacity;
    }

    public void setCubicCapacity(int cubicCapacity) {
        this.cubicCapacity = cubicCapacity;
    }

    public String getYearOfRegistration() {
        return yearOfRegistration;
    }

    public void setYearOfRegistration(String yearOfRegistration) {
        this.yearOfRegistration = yearOfRegistration;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CarBasic carBasic = (CarBasic) o;
        return id == carBasic.id &&
                cubicCapacity == carBasic.cubicCapacity &&
                Objects.equals(model, carBasic.model) &&
                Objects.equals(yearOfRegistration, carBasic.yearOfRegistration);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, model, cubicCapacity, yearOfRegistration);
    }
}
