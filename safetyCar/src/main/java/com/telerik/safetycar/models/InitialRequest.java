package com.telerik.safetycar.models;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "initial_request")
public class InitialRequest {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;

    @OneToOne
    @JoinColumn(name = "car_basic_id")
    private CarBasic carBasic;

    @Column(name = "driver_age")
    private int driverAge;

    @Column(name = "accident")
    private boolean accident = false;

    @Column(name = "quoteEstimate")
    private double premium;

    public InitialRequest() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public CarBasic getCarBasic() {
        return carBasic;
    }

    public void setCarBasic(CarBasic carBasicId) {
        this.carBasic = carBasicId;
    }

    public int getDriverAge() {
        return driverAge;
    }

    public void setDriverAge(int driverAge) {
        this.driverAge = driverAge;
    }

    public boolean isAccident() {
        return accident;
    }

    public void setAccident(boolean accident) {
        this.accident = accident;
    }

    public double getPremium() {
        return premium;
    }

    public void setPremium(double quoteEstimate) {
        this.premium = quoteEstimate;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        InitialRequest that = (InitialRequest) o;
        return id == that.id &&
                driverAge == that.driverAge &&
                accident == that.accident &&
                Double.compare(that.premium, premium) == 0 &&
                Objects.equals(carBasic, that.carBasic);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, carBasic, driverAge, accident, premium);
    }
}
