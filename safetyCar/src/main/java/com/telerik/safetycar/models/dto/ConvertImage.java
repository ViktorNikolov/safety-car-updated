package com.telerik.safetycar.models.dto;

import java.util.Base64;

public class ConvertImage {

    public String convert (byte[] carPicture) {
        return Base64.getMimeEncoder().encodeToString(carPicture);
    }
}
