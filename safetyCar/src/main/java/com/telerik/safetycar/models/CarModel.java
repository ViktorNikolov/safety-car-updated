package com.telerik.safetycar.models;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "cars_models")
public class CarModel {

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @ManyToOne
    @JoinColumn(name = "make_id")
    private CarMake carMake;

    @Column(name = "name")
    private String modelName;

    public CarModel() {
    }

    public CarModel(int id, String modelName) {
        this.id = id;
        this.modelName = modelName;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public CarMake getCarMake() {
        return carMake;
    }

    public void setCarMake(CarMake carMake) {
        this.carMake = carMake;
    }

    public String getModelName() {
        return modelName;
    }

    public void setModelName(String model_name) {
        this.modelName = model_name;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CarModel carModel = (CarModel) o;
        return id == carModel.id &&
                Objects.equals(carMake, carModel.carMake) &&
                Objects.equals(modelName, carModel.modelName);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, carMake, modelName);
    }
}
