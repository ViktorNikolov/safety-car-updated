package com.telerik.safetycar.exceptions;

public class InvalidOperationException extends RuntimeException {

    public InvalidOperationException (String message) {
        super(message);
    }
}
