package com.telerik.safetycar.services;

import com.telerik.safetycar.exceptions.EntityNotFoundException;
import com.telerik.safetycar.exceptions.InvalidOperationException;
import com.telerik.safetycar.models.User;
import com.telerik.safetycar.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserServiceImpl implements UserService {

    private final UserRepository userRepository;

    @Autowired
    public UserServiceImpl(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public List<User> getAll() {
        return userRepository.getAll();
    }

    @Override
    public User getByUsername(String username) {
        User user = userRepository.getByUsername(username);
        if (user == null) {
            throw new EntityNotFoundException(
                    String.format("User with username %s not found.", username));
        }
        return user;
    }

    @Override
    public boolean existsByUsername(String username) {
        return userRepository.existByUsername(username);
    }

    @Override
    public void update(User user, String username) {
        if (!(username.equals(user.getUsername()))) {
            throw new InvalidOperationException(
                    String.format("User %s is not allowed!", username));
        }
        userRepository.update(user);
    }
}
