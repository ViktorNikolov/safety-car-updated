package com.telerik.safetycar.services;

import com.telerik.safetycar.models.Status;

import java.util.List;

public interface StatusService {

    List<Status> getAll();

    Status getById(int id);

}
