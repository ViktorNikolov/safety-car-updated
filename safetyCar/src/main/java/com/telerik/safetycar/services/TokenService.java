package com.telerik.safetycar.services;

import com.telerik.safetycar.models.Token;
import com.telerik.safetycar.models.User;

import java.sql.Timestamp;

public interface TokenService{

    Token findByToken(String token);

    Token findByUser(User user);

    void save(User user, String token);

    Timestamp setExpiry (int timeInMinutes);
}
