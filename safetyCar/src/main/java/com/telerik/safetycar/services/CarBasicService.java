package com.telerik.safetycar.services;

import com.telerik.safetycar.models.CarBasic;

public interface CarBasicService {

    void create(CarBasic carBasic);

    CarBasic getById(int id);

}
