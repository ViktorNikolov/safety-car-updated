package com.telerik.safetycar.services;

import com.telerik.safetycar.models.User;

import java.util.List;

public interface UserService {

    List<User> getAll();

    User getByUsername(String username);

    boolean existsByUsername(String username);

    void update(User user, String username);

}
