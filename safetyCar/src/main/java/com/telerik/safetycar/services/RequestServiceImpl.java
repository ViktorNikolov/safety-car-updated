package com.telerik.safetycar.services;

import com.telerik.safetycar.exceptions.EntityNotFoundException;
import com.telerik.safetycar.exceptions.InvalidOperationException;
import com.telerik.safetycar.models.Request;
import com.telerik.safetycar.models.User;
import com.telerik.safetycar.repositories.RequestRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.mail.MessagingException;
import javax.transaction.Transactional;
import java.util.List;

@Service
public class RequestServiceImpl implements RequestService {

    private final RequestRepository requestRepository;
    private final UserService userService;
    private final EmailService emailService;

    @Autowired
    public RequestServiceImpl(RequestRepository requestRepository, UserService userService, EmailService emailService) {
        this.requestRepository = requestRepository;
        this.userService = userService;
        this.emailService = emailService;
    }

    public Request create(Request request, String username) {
        User user = userService.getByUsername(username);
        if (user == null) {
            throw new EntityNotFoundException(
                    String.format("User with username %s not found.", username));
        }
        if (!(user.getUsername().equals(username))) {
            throw new InvalidOperationException(
                    String.format("User %s is not allowed!", username));
        }
        requestRepository.create(request, username);
        return request;
    }

    @Override
    public List<Request> getAll() {
        return requestRepository.getAll();
    }

    @Override
    public List<Request> getAllPending() {
        return requestRepository.getAllPending();
    }

    @Override
    public List<Request> getByUsername(String username) {
        if (!userService.existsByUsername(username)) {
            throw new EntityNotFoundException(String.format("User with username %s does not exist", username));
        }
        User user = userService.getByUsername(username);
        if (!(user.isAdmin()) && !(user.getUsername().equals(username))) {
            throw new InvalidOperationException(
                    String.format("User %s is not allowed!", username));
        }
        return requestRepository.getByUsername(username);
    }

    @Override
    public Request getById(int id, String username) {
        Request request = requestRepository.getById(id);
        User user = userService.getByUsername(username);
        if (!user.isAdmin() && !(user.getUsername().equalsIgnoreCase(username))) {
            throw new InvalidOperationException(
                    String.format("User %s is not allowed!", user.getUsername()));
        }
        if ((request == null)) {
            throw new EntityNotFoundException(
                    String.format("Request with id %d not found.", id));
        }
        return request;
    }

    @Override
    public List<Request> sortByDate(String username) {
        User user = userService.getByUsername(username);
        if (!user.isAdmin()) {
            throw new InvalidOperationException(
                    String.format("User %s is not allowed!", user.getUsername()));
        }
        return requestRepository.sortByDate();
    }

    @Override
    @Transactional
    public Request update(Request request, String username){
        User user = userService.getByUsername(username);
        if (!user.isAdmin()) {
            throw new InvalidOperationException(
                    String.format("User %s is not allowed to edit the given request", user.getUsername()));
        }
        Request updatedRequest = requestRepository.update(request);
        emailService.sendMailStatus(updatedRequest);
        return updatedRequest;
    }
}

