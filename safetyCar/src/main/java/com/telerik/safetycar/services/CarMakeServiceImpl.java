package com.telerik.safetycar.services;

import com.telerik.safetycar.exceptions.EntityNotFoundException;
import com.telerik.safetycar.models.CarMake;
import com.telerik.safetycar.repositories.CarMakeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service

public class CarMakeServiceImpl implements CarMakeService {
    private final CarMakeRepository makeRepository;

    @Autowired
    public CarMakeServiceImpl(CarMakeRepository makeRepository) {
        this.makeRepository = makeRepository;
    }


    @Override
    public CarMake getById(int id) {
        CarMake make = makeRepository.getById(id);

        if (make == null) {
            throw new EntityNotFoundException(
                    String.format("Car make with id %d not found!", id));
        }

        return make;
    }

    @Override
    public List<CarMake> getAll() {
        return makeRepository.getAll();
    }
}
