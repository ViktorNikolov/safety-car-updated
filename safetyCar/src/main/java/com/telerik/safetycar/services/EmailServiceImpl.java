package com.telerik.safetycar.services;

import com.telerik.safetycar.models.Request;
import com.telerik.safetycar.models.Token;
import com.telerik.safetycar.models.User;
import com.telerik.safetycar.repositories.TokenRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.PropertySource;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;
import org.thymeleaf.context.Context;
import org.thymeleaf.spring5.SpringTemplateEngine;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;


@Service
@PropertySource("classpath:application.properties")
public class EmailServiceImpl implements EmailService{

    private final TokenRepository tokenRepository;
    private final SpringTemplateEngine springTemplateEngine;
    private final JavaMailSender javaMailSender;

    @Autowired
    public EmailServiceImpl(TokenRepository tokenRepository,
                            SpringTemplateEngine springTemplateEngine, JavaMailSender javaMailSender) {
        this.tokenRepository = tokenRepository;
        this.springTemplateEngine = springTemplateEngine;
        this.javaMailSender = javaMailSender;
    }

    @Override
    public void sendMail(User user) throws MessagingException {
        Token token = tokenRepository.findByUser(user);
        String tokenString = token.getToken();
        Context context = new Context ();
        context.setVariable("title", "Please verify your email");
        context.setVariable("link", "http://localhost:8080/activation?token=" + tokenString);
        String body = springTemplateEngine.process("verification", context);
        MimeMessage mimeMessage = javaMailSender.createMimeMessage();
        MimeMessageHelper mimeMessageHelper = new MimeMessageHelper(mimeMessage, true);
        mimeMessageHelper.setTo(user.getUsername());
        mimeMessageHelper.setSubject("email confirmation");
        mimeMessageHelper.setText(body, true);
        javaMailSender.send(mimeMessage);
    }

    public void sendMailStatus(Request request) {
        SimpleMailMessage simpleMailMessage = new SimpleMailMessage();
        simpleMailMessage.setTo(request.getUsername());
        simpleMailMessage.setText(String.format("Change status to %s", request.getStatus().getName()));
        simpleMailMessage.setSubject("Status change");
        javaMailSender.send(simpleMailMessage);
    }
}
