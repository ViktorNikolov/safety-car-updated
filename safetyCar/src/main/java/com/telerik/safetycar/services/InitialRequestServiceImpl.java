package com.telerik.safetycar.services;

import com.telerik.safetycar.exceptions.EntityNotFoundException;
import com.telerik.safetycar.models.InitialRequest;
import com.telerik.safetycar.repositories.BaseQuoteRepository;
import com.telerik.safetycar.repositories.InitialRequestRepository;
import org.springframework.stereotype.Service;

@Service
public class InitialRequestServiceImpl implements InitialRequestService {
    public static final int BASE_DRIVER_AGE_COEFFICIENT = 1;
    public static final int BASE_ACCIDENT_COEFFICIENT = 1;
    public static final double ACCIDENT_COEFFICIENT = 1.2;
    public static final double DRIVER_AGE_COEFFICIENT = 1.05;
    public static final int MIN_AGE = 25;
    public static final double TAX = 1.1;
    private final InitialRequestRepository initialRequestRepository;
    private final BaseQuoteRepository baseQuoteRepository;

    public InitialRequestServiceImpl(InitialRequestRepository initialRequestRepository, BaseQuoteRepository baseQuoteRepository) {
        this.initialRequestRepository = initialRequestRepository;
        this.baseQuoteRepository = baseQuoteRepository;
    }

    @Override
    public void createInitialRequest(InitialRequest initialRequest) {
        initialRequestRepository.createInitialRequest(initialRequest);
    }

    @Override
    public InitialRequest getById(int id) {
        InitialRequest initialRequest = initialRequestRepository.getById(id);
        if (initialRequest == null) {
            throw new EntityNotFoundException(
                    String.format("Initial request with id %d not found!", id));
        }
        return initialRequest;
    }

    @Override
    public double estimatePremium(boolean hadAccidents, int driverAge, int cubicCapacity, int carAge) {
        double accidentCoeff = BASE_ACCIDENT_COEFFICIENT;
        if (hadAccidents) {
            accidentCoeff = ACCIDENT_COEFFICIENT;
        }

        double driverAgeCoeff = BASE_DRIVER_AGE_COEFFICIENT;
        if (driverAge < MIN_AGE) {
            driverAgeCoeff = DRIVER_AGE_COEFFICIENT;
        }

        double baseAmount = baseQuoteRepository.getBaseAmount(cubicCapacity, carAge);

        return baseAmount * accidentCoeff * driverAgeCoeff * TAX;
    }
}
