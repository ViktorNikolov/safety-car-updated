package com.telerik.safetycar.services;

import com.telerik.safetycar.exceptions.EntityNotFoundException;
import com.telerik.safetycar.models.Status;
import com.telerik.safetycar.repositories.StatusRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class StatusServiceImpl implements StatusService {

    private final StatusRepository statusRepository;

    @Autowired
    public StatusServiceImpl(StatusRepository statusRepository) {
        this.statusRepository = statusRepository;
    }

    @Override
    public List<Status> getAll() {
        return statusRepository.getAll();
    }

    @Override
    public Status getById(int id) {
        Status status = statusRepository.getById(id);
        if (status == null) {
            throw new EntityNotFoundException(
                    String.format("Status with Id %d not found.", id));
        }
        return status;
    }

}
