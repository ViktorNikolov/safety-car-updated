package com.telerik.safetycar.services;

import com.telerik.safetycar.models.CarMake;

import java.util.List;

public interface CarMakeService {

    CarMake getById (int id);

    List<CarMake> getAll();
}
