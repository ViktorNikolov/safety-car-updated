package com.telerik.safetycar.services;

import com.telerik.safetycar.models.Request;

import javax.mail.MessagingException;
import java.util.List;

public interface RequestService {

    Request create(Request request, String username);

    List<Request> getAll();

    List<Request> getAllPending();

    List<Request> getByUsername(String username);

    List<Request> sortByDate(String username);

    Request getById(int id, String username);

    Request update(Request request, String username) throws MessagingException;
}
