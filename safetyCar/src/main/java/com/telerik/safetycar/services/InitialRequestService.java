package com.telerik.safetycar.services;

import com.telerik.safetycar.models.InitialRequest;

public interface InitialRequestService {

    void createInitialRequest(InitialRequest initialRequest);

    InitialRequest getById(int id);

    double estimatePremium(boolean hadAccidents, int driverAge, int cubicCapacity, int carAge);

}
