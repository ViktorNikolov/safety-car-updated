package com.telerik.safetycar.services;

import com.telerik.safetycar.models.Token;
import com.telerik.safetycar.models.User;
import com.telerik.safetycar.repositories.TokenRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.sql.Timestamp;
import java.util.Calendar;

@Service
public class TokenServiceImpl implements TokenService {

    private final TokenRepository tokenRepository;


    @Autowired
    public TokenServiceImpl(TokenRepository tokenRepository) {
        this.tokenRepository = tokenRepository;
    }

    @Override
    @Transactional
    public Token findByToken(String token) {
        return tokenRepository.findByToken(token);
    }

    @Override
    @Transactional
    public Token findByUser(User user) {
        return tokenRepository.findByUser(user);
    }

    @Override
    @Transactional
    public void save(User user, String tokenString) {
        Token token = new Token(user, tokenString);
        token.setExpiry(setExpiry(24*60));
        tokenRepository.save(token);
    }

    @Override
    public Timestamp setExpiry(int timeInMinutes) {
        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.MINUTE, timeInMinutes);

        return new Timestamp(calendar.getTime().getTime());
    }
}
