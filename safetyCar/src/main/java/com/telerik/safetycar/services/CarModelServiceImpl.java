package com.telerik.safetycar.services;

import com.telerik.safetycar.exceptions.EntityNotFoundException;
import com.telerik.safetycar.models.CarModel;
import com.telerik.safetycar.repositories.CarModelRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CarModelServiceImpl implements CarModelService {
    private final CarModelRepository repository;

    @Autowired
    public CarModelServiceImpl(CarModelRepository repository) {
        this.repository = repository;
    }

    @Override
    public CarModel getById(int id) {
        CarModel model = repository.getById(id);

        if (model == null) {
            throw new EntityNotFoundException(
                    String.format("Car model with id %d not found!", id));
        }
        return model;
    }

    @Override
    public List<CarModel> getAll() {
        return repository.getAll();
    }

    @Override
    public List<CarModel> getByMakeId(int makeId) {
        return repository.getByMakeId(makeId);
    }


}
