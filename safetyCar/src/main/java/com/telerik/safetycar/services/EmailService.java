package com.telerik.safetycar.services;

import com.telerik.safetycar.models.Request;
import com.telerik.safetycar.models.User;

import javax.mail.MessagingException;

public interface EmailService {

    void sendMail (User user) throws MessagingException;

    void sendMailStatus(Request request);
}
