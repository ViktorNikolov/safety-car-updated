package com.telerik.safetycar.services;

import com.telerik.safetycar.exceptions.EntityNotFoundException;
import com.telerik.safetycar.models.CarBasic;
import com.telerik.safetycar.repositories.CarBasicRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CarBasicServiceImpl implements CarBasicService {

    private final CarBasicRepository carBasicRepository;

    @Autowired
    public CarBasicServiceImpl(CarBasicRepository carBasicRepository) {
        this.carBasicRepository = carBasicRepository;
    }

    @Override
    public void create(CarBasic carBasic) {
        carBasicRepository.create(carBasic);
    }

    @Override
    public CarBasic getById(int id) {
        CarBasic carBasic = carBasicRepository.getById(id);
        if (carBasic == null) {
            throw new EntityNotFoundException(
                    String.format("Car with id %d not found!", id));
        }
        return carBasic;
    }
}
