package com.telerik.safetycar.services;

import com.telerik.safetycar.models.CarModel;

import java.util.List;

public interface CarModelService {

    CarModel getById(int id);

    List<CarModel> getAll();

    List<CarModel> getByMakeId (int makeId);
}
