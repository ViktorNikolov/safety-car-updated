package com.telerik.safetycar.controllers.rest;


import com.telerik.safetycar.exceptions.EntityNotFoundException;
import com.telerik.safetycar.exceptions.InvalidOperationException;
import com.telerik.safetycar.models.Request;
import com.telerik.safetycar.services.RequestService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.mail.MessagingException;
import java.security.Principal;
import java.util.List;

@RestController
@RequestMapping("/api/agent")
public class AgentRestController {

    private final RequestService requestService;

    @Autowired
    public AgentRestController(RequestService requestService) {
        this.requestService = requestService;
    }

    @GetMapping
    public List<Request> getAllRequest() {
        return requestService.getAll();
    }

    @GetMapping("/filterByUsername")
    public List<Request> getAllRequestsByUser(@RequestParam String username) {
        try {
            return requestService.getByUsername(username);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (InvalidOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }

    @GetMapping("/sortByDate")
    public List<Request> sortAllRequestsByDate(Principal principal) {
        try {
            return requestService.sortByDate(principal.getName());
        } catch (InvalidOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }

    @GetMapping("/{id}")
    public Request getById(@PathVariable int id, Principal principal) {
        try {
            return requestService.getById(id, principal.getName());
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (InvalidOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }

    @PutMapping
    public Request update(@RequestBody Request request, Principal principal) throws MessagingException {
        try {
            requestService.update(request, principal.getName());
            return request;
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (InvalidOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }
}
