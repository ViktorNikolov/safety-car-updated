package com.telerik.safetycar.controllers.rest;


import com.telerik.safetycar.exceptions.DuplicateEntityException;
import com.telerik.safetycar.models.User;
import com.telerik.safetycar.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.provisioning.UserDetailsManager;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/api")
public class RegistrationRestController {
    private final UserDetailsManager userDetailsManager;
    private final UserService userService;

    @Autowired
    public RegistrationRestController(UserDetailsManager userDetailsManager, UserService userService) {
        this.userDetailsManager = userDetailsManager;
        this.userService = userService;
    }

    @PostMapping("/register")
    public User registerUser(@Valid @RequestBody User user) {
        try {
            if (userService.getByUsername(user.getUsername()).getUsername().equals(user.getUsername())) {
                List<GrantedAuthority> authorities = AuthorityUtils.createAuthorityList("ROLE_USER");
                org.springframework.security.core.userdetails.User newUser =
                        new org.springframework.security.core.userdetails.User(
                                user.getUsername(), "{noop}" + user.getPassword(), authorities);
                userDetailsManager.createUser(newUser);
            }
        } catch (DuplicateEntityException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        }
        return user;
    }
}
