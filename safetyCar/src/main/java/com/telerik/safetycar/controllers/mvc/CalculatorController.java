package com.telerik.safetycar.controllers.mvc;

import com.telerik.safetycar.exceptions.DuplicateEntityException;
import com.telerik.safetycar.exceptions.EntityNotFoundException;
import com.telerik.safetycar.exceptions.InvalidOperationException;
import com.telerik.safetycar.models.InitialRequest;
import com.telerik.safetycar.models.Request;
import com.telerik.safetycar.models.Status;
import com.telerik.safetycar.models.dto.InitialRequestDTO;
import com.telerik.safetycar.models.dto.RequestDTO;
import com.telerik.safetycar.models.utils.Mapper;
import com.telerik.safetycar.models.utils.InitialRequestMapper;
import com.telerik.safetycar.services.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.io.IOException;
import java.security.Principal;
import java.time.LocalDate;
import java.time.Period;
import java.time.format.DateTimeParseException;

@RequestMapping
@SessionAttributes("initialRequestDTO")
@Controller
public class CalculatorController {

    private final CarMakeService makeService;
    private final CarModelService modelService;
    private final RequestService requestService;
    private final StatusService statusService;
    private final InitialRequestMapper initialRequestMapper;
    private final InitialRequestService initialRequestService;


    @Autowired
    public CalculatorController(
            CarMakeService makeService,
            CarModelService modelService,
            RequestService requestService,
            StatusService statusService, InitialRequestMapper initialRequestMapper,
            InitialRequestService initialRequestService) {

        this.makeService = makeService;
        this.modelService = modelService;
        this.requestService = requestService;
        this.statusService = statusService;
        this.initialRequestMapper = initialRequestMapper;
        this.initialRequestService = initialRequestService;
    }

    @GetMapping("/calculator")
    public String showCalculatorPage(Model model) {
        //for displaying info
        model.addAttribute("makes", makeService.getAll());
        model.addAttribute("models", modelService.getAll());

        //for receiving input
        model.addAttribute("initialRequestDTO", new InitialRequestDTO());

        return "calculator";
    }

    @PostMapping("/calculator")
    public String calculateQuote(@Valid InitialRequestDTO initialRequestDTO, BindingResult bindingResult, Model model) {
        if (bindingResult.hasErrors()) {
            model.addAttribute("error", "Field cannot be empty");
            return "calculator";
        }
        try {
            int carAge = calculateCarAge(initialRequestDTO);
            initialRequestDTO.setPremium(initialRequestService.estimatePremium(
                    initialRequestDTO.isHadAccidents(),
                    initialRequestDTO.getAge(),
                    initialRequestDTO.getCubicCapacity(),
                    carAge));

            initialRequestDTO.setPremiumView(String.format("%.2f", initialRequestDTO.getPremium()));
            model.addAttribute("makes", makeService.getAll());
            model.addAttribute("models", modelService.getAll());

        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (InvalidOperationException e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
        }
        return "calculator";
    }

    @GetMapping("/request")
    public String showRequestPage(Model model) {
        try {
            InitialRequestDTO initialRequestDTO = (InitialRequestDTO) model.getAttribute("initialRequestDTO");
            if (initialRequestDTO == null || initialRequestDTO.getMakeId() == null && initialRequestDTO.getModelId() == null) {

                //for displaying info
                model.addAttribute("makes", makeService.getAll());
                model.addAttribute("models", modelService.getAll());

                //for receiving input
                model.addAttribute("initialRequestDTO", new InitialRequestDTO());

                return "redirect:/calculator";
            }

            model.addAttribute("make", makeService.getById(initialRequestDTO.getMakeId()).getMakeName());
            model.addAttribute("model", modelService.getById(initialRequestDTO.getModelId()).getModelName());

            model.addAttribute("requestDTO", new RequestDTO());
            return "/request";
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (DuplicateEntityException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        } catch (InvalidOperationException e) {
            throw new ResponseStatusException(HttpStatus.FORBIDDEN, e.getMessage());
        } catch (NullPointerException e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
        }
    }

    @PostMapping("/request")
    public String registerRequest(@ModelAttribute InitialRequestDTO initialRequestDTO,
                                  @Valid @ModelAttribute RequestDTO requestDTO,
                                  BindingResult bindingResult,
                                  Principal principal,
                                  Model model) throws IOException {

        if (bindingResult.hasErrors()) {
            model.addAttribute("error", "All fields must be filled");
            return "request";
        }

        try {
            model.getAttribute("initialRequestDTO");
            InitialRequest initialRequest = initialRequestMapper.fromQuoteDTO(initialRequestDTO);

            Request request = Mapper.requestFromDTO(requestDTO, principal);
            request.setInitialRequest(initialRequest);

            Status status = statusService.getById(1);
            request.setStatus(status);

            requestService.create(request, principal.getName());

        } catch (InvalidOperationException e) {
            throw new ResponseStatusException(HttpStatus.FORBIDDEN, e.getMessage());
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
        return "/request-confirmation";
    }

    public int calculateCarAge(InitialRequestDTO initialRequestDTO) {
        try {
            LocalDate currentYear = LocalDate.now();
            String yearOfRegistration = initialRequestDTO.getYearOfRegistration();

            LocalDate dateOfRegistration = LocalDate.parse(yearOfRegistration);

            Period age = Period.between(dateOfRegistration, currentYear);

            return age.getYears();
        } catch (DateTimeParseException e) {
            throw new EntityNotFoundException("Please, fill in date of first registration.");
        }
    }

}
