package com.telerik.safetycar.controllers.rest;

import com.telerik.safetycar.controllers.mvc.CalculatorController;
import com.telerik.safetycar.models.dto.InitialRequestDTO;
import com.telerik.safetycar.services.CarMakeService;
import com.telerik.safetycar.services.CarModelService;
import com.telerik.safetycar.services.InitialRequestService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;


@RestController
@RequestMapping("/api/calculator")
public class CalculatorRestController {
    private final InitialRequestService initialRequestService;
    private final CalculatorController calculatorController;

    @Autowired
    public CalculatorRestController(InitialRequestService initialRequestService, CalculatorController calculatorController) {
        this.initialRequestService = initialRequestService;
        this.calculatorController = calculatorController;
    }

    @GetMapping
    public InitialRequestDTO calculateQuote(@Valid @RequestBody InitialRequestDTO initialRequestDTO) {

            int carAge = calculatorController.calculateCarAge(initialRequestDTO);
            initialRequestDTO.setPremium(initialRequestService.estimatePremium(
                    initialRequestDTO.isHadAccidents(),
                    initialRequestDTO.getAge(),
                    initialRequestDTO.getCubicCapacity(),
                    carAge));
            initialRequestDTO.setPremiumView(String.format("%.2f", initialRequestDTO.getPremium()));
            return initialRequestDTO;
    }

}
