package com.telerik.safetycar.controllers.mvc;

import com.telerik.safetycar.exceptions.DuplicateEntityException;
import com.telerik.safetycar.exceptions.EntityNotFoundException;
import com.telerik.safetycar.exceptions.InvalidOperationException;
import com.telerik.safetycar.models.Request;
import com.telerik.safetycar.models.User;
import com.telerik.safetycar.models.dto.ConvertImage;
import com.telerik.safetycar.models.dto.RequestDTO;
import com.telerik.safetycar.models.dto.UpdateDTO;
import com.telerik.safetycar.models.utils.RequestDisplayMapper;
import com.telerik.safetycar.services.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.io.IOException;
import java.security.Principal;

@Controller
@RequestMapping("/user-profile")
public class UserController {
    private final RequestService requestService;
    private final StatusService statusService;
    private final CarMakeService carMakeService;
    private final CarModelService carModelService;
    private final CarBasicService carBasicService;
    private final InitialRequestService initialRequestService;
    private final RequestDisplayMapper requestDisplayMapper;
    private final UserService userService;

    private final PasswordEncoder encoder;


    @Autowired
    public UserController(RequestService requestService, StatusService statusService,
                          CarMakeService carMakeService, CarModelService carModelService,
                          CarBasicService carBasicService, InitialRequestService initialRequestService,
                          RequestDisplayMapper requestDisplayMapper, UserService userService,
                          PasswordEncoder encoder) {
        this.requestService = requestService;
        this.statusService = statusService;
        this.carMakeService = carMakeService;
        this.carModelService = carModelService;
        this.carBasicService = carBasicService;
        this.initialRequestService = initialRequestService;
        this.requestDisplayMapper = requestDisplayMapper;
        this.userService = userService;
        this.encoder = encoder;
    }

    @GetMapping
    public String showUser(Model model, Principal principal) {

        String username = principal.getName();
        User user = userService.getByUsername(username);

        model.addAttribute("user", user);
        model.addAttribute("request", requestService.getByUsername(principal.getName()));
        model.addAttribute("requestDTO", new RequestDTO());

        return "user-profile";
    }

    @GetMapping("/{username}/edit")
    public String showEditUSerPage(@PathVariable String username,
                                   @Valid @ModelAttribute UpdateDTO updateDTO,
                                   BindingResult bindingResult,
                                   Model model) {
        if (bindingResult.hasErrors()) {
            model.addAttribute("error", "Username/password cannot be empty");
            return "index";
        }
        try {
            User user = userService.getByUsername(username);
            model.addAttribute("user", user);
            updateDTO.setPassword(user.getPassword());
            model.addAttribute("updateDTO", updateDTO);
            return "user-update";

        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }

    }

    @PostMapping("{username}/edit")
    public String handleEditUserPage(@PathVariable String username,
                                     @Valid @ModelAttribute UpdateDTO updateDTO,
                                     BindingResult bindingResult,
                                     Principal principal,
                                     Model model) {

        if (bindingResult.hasErrors()) {
            model.addAttribute("error", "Username/password cannot be empty");
            return "user-update";
        }

        try {
            User userToUpdate = userService.getByUsername(username);
            String encoded = encoder.encode(updateDTO.getPassword());

            userToUpdate.setPassword(encoded);
            userService.update(userToUpdate, principal.getName());

            return "index";
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (DuplicateEntityException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        } catch (InvalidOperationException e) {
            throw new ResponseStatusException(HttpStatus.FORBIDDEN, e.getMessage());
        }
    }

    @GetMapping("/my-requests")
    public String showUserRequests(Model model, Principal principal) {

        model.addAttribute("requests", requestService.getByUsername(principal.getName()));
        model.addAttribute("picture", new ConvertImage());

        return "my-requests";
    }

    @GetMapping("/my-requests/{id}")
    public String getRequestById(@PathVariable int id, @ModelAttribute RequestDTO requestDTO,
                                 BindingResult bindingResult, Model model, Principal principal) throws IOException {
        if (hasError(bindingResult, model)) return "my-requests";

        try {
            Request request = requestService.getById(id, principal.getName());

            requestDTO = requestDisplayMapper.toDto(id, requestDTO, principal, requestService, statusService,
                    carMakeService, carModelService, carBasicService, initialRequestService);

            model.addAttribute("requestDTO", requestDTO);
            model.addAttribute("request", request);
            model.addAttribute("picture", new ConvertImage());


        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (InvalidOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        } catch (IOException e) {
            throw new IOException("");
        }
        return "handled-my";
    }

    private boolean hasError(BindingResult bindingResult, Model model) {
        if (bindingResult.hasErrors()) {
            model.addAttribute("error", "All fields must be filled");
            return true;
        }
        return false;
    }
}
