package com.telerik.safetycar.controllers.rest;

import com.telerik.safetycar.exceptions.EntityNotFoundException;
import com.telerik.safetycar.models.CarMake;
import com.telerik.safetycar.models.CarModel;
import com.telerik.safetycar.services.CarModelService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;

@RestController
@RequestMapping("/api/models")
public class ModelsRestController {
    private final CarModelService modelService;

    @Autowired
    public ModelsRestController(CarModelService modelService) {
        this.modelService = modelService;
    }

    @GetMapping
    public List<CarModel> getAllModels() {
        return modelService.getAll();
    }

    @GetMapping("/{id}")
    public CarModel getById(@PathVariable int id) {
        try {
            return modelService.getById(id);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @GetMapping("/make/{id}")
    public List<CarModel> getModelsByMakeId(@PathVariable int id) {
        try {
            return modelService.getByMakeId(id);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }
}

