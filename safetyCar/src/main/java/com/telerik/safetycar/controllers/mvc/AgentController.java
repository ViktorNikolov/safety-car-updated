package com.telerik.safetycar.controllers.mvc;

import com.telerik.safetycar.exceptions.EntityNotFoundException;
import com.telerik.safetycar.exceptions.InvalidOperationException;
import com.telerik.safetycar.models.Status;
import com.telerik.safetycar.models.dto.ConvertImage;
import com.telerik.safetycar.models.utils.RequestDisplayMapper;
import com.telerik.safetycar.models.Request;
import com.telerik.safetycar.models.dto.RequestDTO;
import com.telerik.safetycar.services.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.mail.MessagingException;
import java.io.IOException;
import java.security.Principal;

@Controller
@RequestMapping("/agent")
public class AgentController {
    private final RequestService requestService;
    private final StatusService statusService;
    private final CarMakeService carMakeService;
    private final CarModelService carModelService;
    private final CarBasicService carBasicService;
    private final InitialRequestService initialRequestService;
    private final RequestDisplayMapper requestDisplayMapper;
    private final MyErrorController errorController;

    @Autowired
    public AgentController(RequestService requestService, StatusService statusService,
                           CarMakeService carMakeService, CarModelService carModelService,
                           CarBasicService carBasicService, InitialRequestService initialRequestService,
                           RequestDisplayMapper requestDisplayMapper, MyErrorController errorController) {
        this.requestService = requestService;
        this.statusService = statusService;
        this.carMakeService = carMakeService;
        this.carModelService = carModelService;
        this.carBasicService = carBasicService;
        this.initialRequestService = initialRequestService;
        this.requestDisplayMapper = requestDisplayMapper;
        this.errorController = errorController;
    }

    @GetMapping
    public String getAll(Model model) {

        model.addAttribute("requests", requestService.getAllPending());
        model.addAttribute("requestDTO", new RequestDTO());
        model.addAttribute("picture", new ConvertImage());

        return "agent";
    }

    @GetMapping("/{id}")
    public String getRequestById(@PathVariable int id, @ModelAttribute RequestDTO requestDTO,
                                 BindingResult bindingResult, Model model, Principal principal) throws IOException {
        if (hasError(bindingResult, model)) return "agent";

        try {
            Request request = requestService.getById(id, principal.getName());

            requestDisplayMapper.toDto(id, requestDTO, principal, requestService, statusService,
                    carMakeService, carModelService, carBasicService, initialRequestService);

            model.addAttribute("requestDTO", requestDTO);
            model.addAttribute("request", request);
            model.addAttribute("statuses", statusService.getAll());
            model.addAttribute("picture", new ConvertImage());


        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (InvalidOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        } catch (IOException e) {
            throw new IOException();
        }
        return "handled-request";
    }

    @PostMapping("/{id}")
    public String handleRequestById(@PathVariable int id, @ModelAttribute RequestDTO requestDTO,
                                    BindingResult bindingResult, Model model, Principal principal) throws MessagingException {
        if (hasError(bindingResult, model)) return "agent";

        try {
            Status status = statusService.getById(requestDTO.getStatusId());
            Request requestToUpdate = requestService.getById(id, principal.getName());
            requestToUpdate.setStatus(status);

            requestService.update(requestToUpdate, principal.getName());
            model.addAttribute("picture", new ConvertImage());

            return "redirect:/agent";

        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (InvalidOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }

    @PostMapping
    public String getRequestsByUsername(@ModelAttribute RequestDTO requestDTO, BindingResult bindingResult,
                                        String username, Model model) {
        try {
            if (hasError(bindingResult, model)) return "agent";

            model.addAttribute("requests", requestService.getByUsername(username));
            model.addAttribute("picture", new ConvertImage());
            return "agent";

        } catch (InvalidOperationException | EntityNotFoundException e) {
            model.addAttribute("errorMessage", e.getMessage());
            return "agent";
        }
    }

    @GetMapping("/sortByDate")
    public String getRequestsByDates(@ModelAttribute RequestDTO requestDTO, BindingResult bindingResult, Model model, Principal principal) {
        if (hasError(bindingResult, model)) return "agent";
        try {
            model.addAttribute("requests", requestService.sortByDate(principal.getName()));
            model.addAttribute("picture", new ConvertImage());

            return "agent";
        } catch (InvalidOperationException e) {
            return "redirect:/";
        }
    }

    private boolean hasError(BindingResult bindingResult, Model model) {
        if (bindingResult.hasErrors()) {
            model.addAttribute("error", "All fields must be filled");
            return true;
        }
        return false;
    }
}
