package com.telerik.safetycar.controllers.mvc;

import com.telerik.safetycar.exceptions.EntityNotFoundException;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice
public class MyErrorController {

    @ExceptionHandler
    public String showErrorPage(EntityNotFoundException e, Model model) {

        model.addAttribute("message", e.getMessage());

        return "error";
    }
}
