package com.telerik.safetycar.controllers.rest;

import com.telerik.safetycar.exceptions.EntityNotFoundException;
import com.telerik.safetycar.models.CarMake;
import com.telerik.safetycar.models.CarModel;
import com.telerik.safetycar.services.CarMakeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;

@RestController
@RequestMapping("/api/makes")
public class MakesRestController {
    private final CarMakeService makeService;


    @Autowired
    public MakesRestController(CarMakeService makeService) {
        this.makeService = makeService;
    }

    @GetMapping
    public List<CarMake> getAllMakes() {
        return makeService.getAll();
    }

    @GetMapping("/{id}")
    public CarMake getById(@PathVariable int id) {
        try {
            return makeService.getById(id);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }


}
