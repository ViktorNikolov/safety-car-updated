package com.telerik.safetycar.controllers.rest;

import com.telerik.safetycar.exceptions.EntityNotFoundException;
import com.telerik.safetycar.models.User;
import com.telerik.safetycar.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;

@RestController
@RequestMapping("/api/users")
public class UserRestController {
    private final UserService userService;

    @Autowired
    public UserRestController(UserService userService) {
        this.userService = userService;
    }

    @GetMapping
    public List<User> showAll() {
        return userService.getAll();
    }

    @GetMapping("/{username}")
    public User showByUsername(@PathVariable String username) {
        User user;
        try {
            user = userService.getByUsername(username);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
        return user;
    }

    @PutMapping("/{username}")
    public User update(@PathVariable String username) {
        User user;
        try {
            user = userService.getByUsername(username);
            userService.update(user, username);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
        return user;
    }

    @DeleteMapping("/{username}")
    public void delete(@PathVariable String username) {
        User user;
        try {
            user = userService.getByUsername(username);
            userService.update(user, username);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }
}