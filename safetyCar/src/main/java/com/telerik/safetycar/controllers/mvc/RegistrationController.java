package com.telerik.safetycar.controllers.mvc;

import com.telerik.safetycar.models.Token;
import com.telerik.safetycar.models.User;
import com.telerik.safetycar.models.dto.UserRegistrationDTO;
import com.telerik.safetycar.services.EmailService;
import com.telerik.safetycar.services.TokenService;
import com.telerik.safetycar.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.provisioning.UserDetailsManager;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.mail.MessagingException;
import javax.validation.Valid;
import java.sql.Timestamp;
import java.util.List;
import java.util.UUID;

@Controller
public class RegistrationController {
    private final UserDetailsManager userDetailsManager;
    private final PasswordEncoder encoder;
    private final EmailService emailService;
    private final UserService userService;
    private final TokenService tokenService;

    @Autowired
    public RegistrationController(UserDetailsManager userDetailsManager, PasswordEncoder encoder, EmailService emailService, UserService userService, TokenService tokenService) {
        this.userDetailsManager = userDetailsManager;
        this.encoder = encoder;
        this.emailService = emailService;
        this.userService = userService;
        this.tokenService = tokenService;
    }

    @GetMapping("/register")
    public String showRegisterPage(Model model) {
        model.addAttribute("user", new UserRegistrationDTO());
        return "register";
    }

    @PostMapping("/register")
    public String registerUser(
            @Valid @ModelAttribute UserRegistrationDTO userDto,
            BindingResult bindingResult,
            Model model) throws MessagingException {

        if (bindingResult.hasErrors()) {
            model.addAttribute("user", userDto);
            model.addAttribute("error", "Username/password cannot be empty");
            return "register";
        }

        if (userDetailsManager.userExists(userDto.getUsername())) {
            model.addAttribute("user", userDto);
            model.addAttribute("error", "User with the same email already exists!");
            return "register";
        }

        if (!userDto.getPassword().equals(userDto.getPasswordConfirmation())) {
            model.addAttribute("user", userDto);
            model.addAttribute("error", "Password confirmation doesn't match password!");
            return "register";
        }

        List<GrantedAuthority> authorities = AuthorityUtils.createAuthorityList("ROLE_USER");
        String encoded = encoder.encode(userDto.getPassword());
        org.springframework.security.core.userdetails.User newUser =
                new org.springframework.security.core.userdetails.User(
                        userDto.getUsername(), encoded, authorities);

        userDetailsManager.createUser(newUser);
        User user = userService.getByUsername(newUser.getUsername());
        user.setEnabled(false);
        userService.update(user, user.getUsername());
        String token = UUID.randomUUID().toString();
        tokenService.save(user, token);
        emailService.sendMail(user);

        return "/email-confirmation";
    }

    @GetMapping("/activation")
    public String activation(@RequestParam("token") String tokenString, Model model) {
        Token token = tokenService.findByToken(tokenString);
        User user = token.getUser();
        if (!user.isEnabled()) {
            Timestamp current = new Timestamp(System.currentTimeMillis());
            if (token.getExpiry().before(current)) {
                model.addAttribute("message", "your token has expired");
            } else {
                user.setEnabled(true);
                userService.update(user, user.getUsername());
                model.addAttribute("message", "Your account has been activate successfully");
            }
        }
        return "register-confirmation";
    }
}
