package com.telerik.safetycar.services;

import com.telerik.safetycar.exceptions.EntityNotFoundException;
import com.telerik.safetycar.models.CarModel;
import com.telerik.safetycar.repositories.CarModelRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
public class CarModelServiceImplTests {


    @InjectMocks
    CarModelServiceImpl modelService;

    @Mock
    CarModelRepository mockRepository;


    @Test
    public void getById_Should_ReturnModel_When_ModelExists() {
        //Arrange
        Mockito.when(mockRepository.getById(Mockito.anyInt())).thenReturn(new CarModel());

        //Act
        CarModel result = modelService.getById(Mockito.anyInt());

        //Assert
        Assertions.assertEquals(new CarModel(), result);
    }

    @Test
    public void getById_ShouldTrow_When_InvalidModelID () {
        Mockito.when(mockRepository.getById(Mockito.anyInt()))
                .thenReturn(null);

        Assertions.assertThrows(EntityNotFoundException.class,
                () -> modelService.getById(1));
    }

}
