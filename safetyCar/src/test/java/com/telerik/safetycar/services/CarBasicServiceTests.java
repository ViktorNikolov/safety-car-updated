package com.telerik.safetycar.services;

import com.telerik.safetycar.exceptions.EntityNotFoundException;
import com.telerik.safetycar.models.CarBasic;
import com.telerik.safetycar.repositories.CarBasicRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)

public class CarBasicServiceTests {
    @InjectMocks
    CarBasicServiceImpl carBasicService;
    @Mock
    CarBasicRepository mockCarBasicRepository;


    @Test
    public void getById_ShouldReturn_WhenValidId() {
        // Arrange
        CarBasic carBasic = new CarBasic();
        Mockito.when(mockCarBasicRepository.getById(Mockito.anyInt()))
                .thenReturn(carBasic);

        // Act
        CarBasic carBasicServiceById = carBasicService.getById(1);

        // Assert
        Assertions.assertEquals(carBasic, carBasicServiceById);
    }

    @Test
    public void getById_ShouldThrow_WhenInvalidId() {
        Mockito.when(mockCarBasicRepository.getById(Mockito.anyInt()))
                .thenReturn(null);

        Assertions.assertThrows(EntityNotFoundException.class,
                () -> carBasicService.getById(1));
    }

    @Test
    public void create_ShouldReturn_WhenIsValid() {
        // Arrange
        CarBasic carBasic = new CarBasic();

        // Act
        carBasicService.create(carBasic);

        // Assert
        Mockito.verify(mockCarBasicRepository,
                Mockito.times(1)).create(carBasic);
    }

}
