package com.telerik.safetycar.services;

import com.telerik.safetycar.models.Authority;
import com.telerik.safetycar.models.User;
import org.springframework.stereotype.Component;

import java.util.HashSet;
import java.util.Set;

@Component
public class Helper {

    public static void setToAdmin(User mockUser) {
        Set<Authority> authoritySet = new HashSet<>();
        Authority authority = new Authority();
        authoritySet.add(authority);
        authority.setUsername("ADMIN");
        authority.setAuthority("ROLE_ADMIN");
        mockUser.setUsername("admin");
        mockUser.setAuthorities(authoritySet);
    }

    public static void setToUser(User mockUser) {
        Set<Authority> authoritySet = new HashSet<>();
        Authority authority = new Authority();
        authoritySet.add(authority);
        authority.setUsername("USER");
        authority.setAuthority("ROLE_USER");
        mockUser.setUsername("user");
        mockUser.setAuthorities(authoritySet);
    }

    public static void setToAnonymous(User mockUser) {
        Set<Authority> authoritySet = new HashSet<>();
        Authority authority = new Authority();
        authoritySet.add(authority);
        authority.setUsername("USER");
        authority.setAuthority("ROLE_ANONYMOUS");
        mockUser.setUsername("user");
        mockUser.setAuthorities(authoritySet);
    }
}
