package com.telerik.safetycar.services;

import com.telerik.safetycar.exceptions.EntityNotFoundException;
import com.telerik.safetycar.models.Status;
import com.telerik.safetycar.repositories.StatusRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
public class StatusServiceTests {

    @InjectMocks
    StatusServiceImpl statusService;

    @Mock
    StatusRepository mockStatusRepository;

    @Test
    public void getAll_ShouldReturn_WhenInvalidId() {
        // Act
        statusService.getAll();
        // Assert
        Mockito.verify(mockStatusRepository,
                Mockito.times(1)).getAll();
    }

    @Test
    public void getById_ShouldThrow_WhenInvalidId() {
        // Arrange
        Mockito.when(mockStatusRepository.getById(Mockito.anyInt()))
                .thenReturn(null);

        // Act, Assert
        Assertions.assertThrows(EntityNotFoundException.class,
                () -> statusService.getById(1));
    }

    @Test
    public void getById_ShouldReturn_WhenValidId() {
        // Arrange
        Status status = new Status();
        Mockito.when(mockStatusRepository.getById(Mockito.anyInt()))
                .thenReturn(status);

        // Act
        Status returnedStatus = statusService.getById(1);

        // Assert
        Assertions.assertEquals(returnedStatus, status);
    }
}
