package com.telerik.safetycar.services;

import com.telerik.safetycar.exceptions.EntityNotFoundException;
import com.telerik.safetycar.models.CarMake;
import com.telerik.safetycar.repositories.CarMakeRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
public class CarMakeServiceImplTests {

    @InjectMocks
    CarMakeServiceImpl makeService;
    @Mock
    CarMakeRepository mockRepository;

    @Test
    public void getById_Should_ReturnMake_When_MakeExists() {
        //Arrange
        Mockito.when(mockRepository.getById(Mockito.anyInt())).thenReturn(new CarMake());

        //Act
        CarMake result = makeService.getById(Mockito.anyInt());

        //Assert
        Assertions.assertEquals(new CarMake(), result);

    }

    @Test
    public void getById_ShouldTrow_When_InvalidMakeID() {
        Mockito.when(mockRepository.getById(Mockito.anyInt()))
                .thenReturn(null);

        Assertions.assertThrows(EntityNotFoundException.class, () -> makeService.getById(1));
    }


}
