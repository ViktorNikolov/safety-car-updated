package com.telerik.safetycar.services;

import com.telerik.safetycar.exceptions.EntityNotFoundException;
import com.telerik.safetycar.exceptions.InvalidOperationException;
import com.telerik.safetycar.models.Authority;
import com.telerik.safetycar.models.Request;
import com.telerik.safetycar.models.User;
import com.telerik.safetycar.repositories.RequestRepository;
import com.telerik.safetycar.repositories.UserRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.mail.MessagingException;
import java.security.Principal;
import java.util.HashSet;
import java.util.Set;

import static org.mockito.Mockito.mock;

@ExtendWith(MockitoExtension.class)
public class RequestServiceTests {

    @InjectMocks
    RequestServiceImpl requestService;

    @Mock
    RequestRepository mockRequestRepository;

    @Mock
    UserService mockUserService;

    @Mock
    EmailService mockEmailService;

    @Test
    public void getAll_ShouldReturn_WhenInvalidId() {
        // Act
        requestService.getAll();
        // Assert
        Mockito.verify(mockRequestRepository,
                Mockito.times(1)).getAll();
    }

    @Test
    public void getAllPending_ShouldReturn_WhenInvalidId() {
        // Act
        requestService.getAllPending();
        // Assert
        Mockito.verify(mockRequestRepository,
                Mockito.times(1)).getAllPending();
    }

    @Test
    public void create_ShouldThrow_WhenIsNotUser() {
        // Arrange
        Request testRequest = new Request();

        Mockito.when(mockUserService.getByUsername(Mockito.anyString()))
                .thenReturn(null);

        // Act, Assert
        Assertions.assertThrows(EntityNotFoundException.class,
                () -> requestService.create(testRequest, Mockito.anyString()));

        Mockito.verify(mockRequestRepository,
                Mockito.times(0))
                .create(testRequest, "user");
    }

    @Test
    public void create_ShouldThrow_WhenIsOtherUser() {
        // Arrange
        User mockUser = new User();
        Mockito.when(mockUserService.getByUsername(Mockito.anyString()))
                .thenReturn(mockUser);
        Helper.setToUser(mockUser);
        Request testRequest = new Request();

        // Act, Assert
        Assertions.assertThrows(InvalidOperationException.class,
                () -> requestService.create(testRequest, "test"));

        Mockito.verify(mockRequestRepository,
                Mockito.times(0))
                .create(testRequest, "user");
    }

    @Test
    public void create_ShouldCreate_WhenIsUser() {
        // Arrange
        User mockUser = new User();
        Mockito.when(mockUserService.getByUsername(Mockito.anyString()))
                .thenReturn(mockUser);
        Helper.setToUser(mockUser);
        Request testRequest = new Request();

        // Act
        requestService.create(testRequest, mockUser.getUsername());

        // Assert
        Mockito.verify(mockRequestRepository,
                Mockito.times(1)).create(testRequest, mockUser.getUsername());
    }

    @Test
    public void update_ShouldThrow_WhenIsNotAdmin() {
        // Arrange
        User mockUser = new User();
        Mockito.when(mockUserService.getByUsername(Mockito.anyString()))
                .thenReturn(mockUser);
        Helper.setToUser(mockUser);
        Request testRequest = new Request();


        // Act, Assert
        Assertions.assertThrows(InvalidOperationException.class,
                () -> requestService.update(testRequest, Mockito.anyString()));

        Mockito.verify(mockRequestRepository,
                Mockito.times(0))
                .update(testRequest);
    }

    @Test
    public void update_ShouldUpdate_WhenIsAdmin() {
        // Arrange
        User mockUser = new User();
        Mockito.when(mockUserService.getByUsername(Mockito.anyString()))
                .thenReturn(mockUser);
        Helper.setToAdmin(mockUser);
        Request testRequest = new Request();
        testRequest.setUsername(mockUser.getUsername());

        // Act
        requestService.update(testRequest, mockUser.getUsername());

        // Assert
        Mockito.verify(mockRequestRepository,
                Mockito.times(1)).update(testRequest);
    }

    @Test
    public void getById_ShouldThrow_WhenInvalidId() {
        // Arrange
        User mockUser = new User();
        Mockito.when(mockUserService.getByUsername(Mockito.anyString()))
                .thenReturn(mockUser);
        Helper.setToAdmin(mockUser);
        Mockito.when(mockRequestRepository.getById(Mockito.anyInt()))
                .thenReturn(null);

        // Act, Assert
        Assertions.assertThrows(EntityNotFoundException.class,
                () -> requestService.getById(1, mockUser.getUsername()));
    }

    @Test
    public void getById_ShouldThrow_WhenIsNotAdmin() {
        // Arrange
        User mockUser = new User();
        Mockito.when(mockUserService.getByUsername(Mockito.anyString()))
                .thenReturn(mockUser);
        Helper.setToUser(mockUser);

        // Act, Assert
        Assertions.assertThrows(InvalidOperationException.class,
                () -> requestService.getById(1, Mockito.anyString()));
    }

    @Test
    public void getById_ShouldThrow_WhenIsNotUser() {
        //        // Arrange
        User mockUser = new User();
        Mockito.when(mockUserService.getByUsername(Mockito.anyString()))
                .thenReturn(mockUser);
        Helper.setToAnonymous(mockUser);

        // Act, Assert
        Assertions.assertThrows(InvalidOperationException.class,
                () -> requestService.getById(1, "test"));
    }

    @Test
    public void getById_ShouldReturn_WhenValidId() {
        // Arrange
        User mockUser = new User();
        Mockito.when(mockUserService.getByUsername(Mockito.anyString()))
                .thenReturn(mockUser);
        Helper.setToAdmin(mockUser);
        Request request = new Request();
        Mockito.when(mockRequestRepository.getById(Mockito.anyInt()))
                .thenReturn(request);

        // Act
        Request returnedRequest = requestService.getById(1, mockUser.getUsername());

        // Assert
        Assertions.assertEquals(returnedRequest, request);
    }

    @Test
    public void getByUsername_ShouldReturn_WhenIsUser() {
        // Arrange
        User mockUser = new User();
        Mockito.when(mockUserService.existsByUsername(Mockito.anyString()))
                .thenReturn(true);
        Mockito.when(mockUserService.getByUsername(Mockito.anyString()))
                .thenReturn(mockUser);
        Helper.setToUser(mockUser);

        // Act
        requestService.getByUsername(mockUser.getUsername());

        // Assert
        Mockito.verify(mockRequestRepository,
                Mockito.times(1)).getByUsername(mockUser.getUsername());
    }

    @Test
    public void getByUsername_ShouldReturn_WhenIsAdmin() {
        // Arrange
        User mockUser = new User();
        Mockito.when(mockUserService.existsByUsername(Mockito.anyString()))
                .thenReturn(true);
        Mockito.when(mockUserService.getByUsername(Mockito.anyString()))
                .thenReturn(mockUser);
        Helper.setToAdmin(mockUser);

        // Act
        requestService.getByUsername(mockUser.getUsername());

        // Assert
        Mockito.verify(mockRequestRepository,
                Mockito.times(1)).getByUsername(mockUser.getUsername());
    }

    @Test
    public void getByUsername_ShouldThrow_WhenIsNotUser() {
        // Arrange
        User mockUser = new User();
        Mockito.when(mockUserService.existsByUsername(Mockito.anyString()))
                .thenReturn(false);
        Helper.setToUser(mockUser);

        // Act  // Assert
        Assertions.assertThrows(EntityNotFoundException.class,
                () -> requestService.getByUsername(Mockito.anyString()));

        Mockito.verify(mockRequestRepository,
                Mockito.times(0)).getByUsername(Mockito.anyString());
    }

    @Test
    public void getByUsername_ShouldThrow_WhenIsNotAdmin() {
        // Arrange
        User mockUser = new User();
        Mockito.when(mockUserService.existsByUsername(Mockito.anyString()))
                .thenReturn(true);
        Mockito.when(mockUserService.getByUsername(Mockito.anyString()))
                .thenReturn(mockUser);
        Helper.setToUser(mockUser);

        // Act  // Assert
        Assertions.assertThrows(InvalidOperationException.class,
                () -> requestService.getByUsername(Mockito.anyString()));

        Mockito.verify(mockRequestRepository,
                Mockito.times(0)).getByUsername(Mockito.anyString());
    }

    @Test
    public void sortByDate_ShouldReturn_WhenIsAdmin() {
        // Arrange
        User mockUser = new User();
        Mockito.when(mockUserService.getByUsername(Mockito.anyString()))
                .thenReturn(mockUser);
        Helper.setToAdmin(mockUser);

        // Act
        requestService.sortByDate(mockUser.getUsername());

        // Assert
        Mockito.verify(mockRequestRepository,
                Mockito.times(1)).sortByDate();
    }

    @Test
    public void sortByDate_ShouldThrow_WhenIsNotUser() {
        // Arrange
        User mockUser = new User();
        Mockito.when(mockUserService.getByUsername(Mockito.anyString()))
                .thenReturn(mockUser);
        Helper.setToUser(mockUser);

        // Act  // Assert
        Assertions.assertThrows(InvalidOperationException.class,
                () -> requestService.sortByDate(Mockito.anyString()));

        Mockito.verify(mockRequestRepository,
                Mockito.times(0)).sortByDate();
    }
}
