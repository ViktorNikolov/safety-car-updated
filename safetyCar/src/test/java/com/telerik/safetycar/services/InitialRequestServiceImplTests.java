package com.telerik.safetycar.services;

import com.telerik.safetycar.exceptions.EntityNotFoundException;
import com.telerik.safetycar.models.InitialRequest;
import com.telerik.safetycar.repositories.InitialRequestRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
public class InitialRequestServiceImplTests {

    @InjectMocks
    InitialRequestServiceImpl initialRequestService;

    @Mock
    InitialRequestRepository mockInitialRequestRepository;

    @Test
    public void create_ShouldReturn_WhenIsValid() {
        // Arrange
        InitialRequest initialRequest = new InitialRequest();

        // Act
        initialRequestService.createInitialRequest(initialRequest);

        // Assert
        Mockito.verify(mockInitialRequestRepository,
                Mockito.times(1)).createInitialRequest(initialRequest);
    }

    @Test
    public void getById_ShouldReturn_WhenValidId() {
        // Arrange
        InitialRequest initialRequest = new InitialRequest();
        Mockito.when(mockInitialRequestRepository.getById(Mockito.anyInt()))
                .thenReturn(initialRequest);

        // Act
        InitialRequest initialRequestById = initialRequestService.getById(1);

        // Assert
        Assertions.assertEquals(initialRequest, initialRequestById);
    }

    @Test
    public void getById_ShouldTrow_When_NonExistentId () {
        Mockito.when(mockInitialRequestRepository.getById(Mockito.anyInt()))
                .thenReturn(null);

        Assertions.assertThrows(EntityNotFoundException.class,
                () -> initialRequestService.getById(1));
    }

}
