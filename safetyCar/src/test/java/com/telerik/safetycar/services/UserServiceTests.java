package com.telerik.safetycar.services;

import com.telerik.safetycar.exceptions.EntityNotFoundException;
import com.telerik.safetycar.exceptions.InvalidOperationException;
import com.telerik.safetycar.models.User;
import com.telerik.safetycar.repositories.UserRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;


@ExtendWith(MockitoExtension.class)
public class UserServiceTests {

    @InjectMocks
    UserServiceImpl userService;

    @Mock
    UserRepository mockUserRepository;

    @Test
    public void getAll_ShouldReturn_WhenInvalidId() {
        // Arrange

        // Act
        userService.getAll();
        // Assert
        Mockito.verify(mockUserRepository,
                Mockito.times(1)).getAll();
    }

    @Test
    public void getByUsername_ShouldThrow_WhenIsNotPresent() {
        // Arrange
        Mockito.when(mockUserRepository.getByUsername(Mockito.anyString()))
                .thenReturn(null);

        // Act, Assert
        Assertions.assertThrows(EntityNotFoundException.class,
                () -> userService.getByUsername(Mockito.anyString()));
    }

    @Test
    public void getByUsername_ShouldReturn_WhenIsPresent() {
        // Arrange
        User mockUser = new User();
        Mockito.when(mockUserRepository.getByUsername(Mockito.anyString()))
                .thenReturn(mockUser);

        // Act
        User returnedUSer = userService.getByUsername(Mockito.anyString());

        // Assert
        Assertions.assertEquals(mockUser, returnedUSer);
    }

    @Test
    public void update_ShouldThrow_WhenIsNotAllowed() {
        // Arrange
        User testUser = new User();
        Helper.setToUser(testUser);

        // Act, Assert
        Assertions.assertThrows(InvalidOperationException.class,
                () -> userService.update(testUser, "test"));

        Mockito.verify(mockUserRepository,
                Mockito.times(0))
                .update(testUser);
    }

    @Test
    public void update_ShouldUpdate_WhenIsAllowed() {
        // Arrange
        User testUser = new User();
        Helper.setToUser(testUser);

        // Act
        userService.update(testUser, testUser.getUsername());

        // Assert
        Mockito.verify(mockUserRepository,
                Mockito.times(1)).update(testUser);
    }

    @Test
    public void existsByUsername_ShouldTrue_WhenIsExist() {
        // Arrange
        Mockito.when(mockUserRepository.existByUsername(Mockito.anyString()))
                .thenReturn(true);

        // Act, Assert
        Assertions.assertTrue(userService.existsByUsername(Mockito.anyString()));
    }

    @Test
    public void existsByUsername_ShouldFalse_WhenIsNotExist() {
        // Arrange
        Mockito.when(mockUserRepository.existByUsername(Mockito.anyString()))
                .thenReturn(false);

        // Act, Assert
        Assertions.assertFalse(userService.existsByUsername(Mockito.anyString()));
    }
}
